defmodule HockeyTest do
  use ExUnit.Case
  doctest Hockey

  import Hockey

  def single_team() do
    """
    <tr class="team">
        <td class="name">
            Boston Bruins
        </td>
        <td class="year">
            1990
        </td>
        <td class="wins">
            44
        </td>
        <td class="losses">
            24
        </td>
        <td class="ot-losses">
            
        </td>
        
        
        <td class="pct text-success">
            0.55
        </td>
        
        <td class="gf">
            299
        </td>
        <td class="ga">
            264
        </td>
        
        
        <td class="diff text-success">
            35
        </td>
        
    </tr>
    """
  end

  test "Creating page 0 URL works" do
    url = Hockey.page_number_to_url(0)
    assert url == "https://scrapethissite.com/pages/forms/"
  end

  test "Create page numbers > 0 works as expected" do
    url = Hockey.page_number_to_url(4)
    assert url == "https://scrapethissite.com/pages/forms/?page_num=4"
  end

  test "Floki can parse class selectors with embedded spaces" do
    html = """
    <tr class="team">
       <td class="name">
         Boston Bruins
       </td>
       <td class="ot-losses">
                            
       </td>
              
        <td class="diff text-success">
           99
        </td>
    </tr>
    """

    {:ok, parsed} = Floki.parse_document(html)
    #IO.puts("parsed=#{inspect(parsed)}")
    ateam = Hockey.pull_page_data(parsed)
    #IO.puts("ateam:#{inspect(ateam)}")

    expected = %Hockey{
      "diff text-success": "99",
      ga: nil,
      gf: nil,
      losses: nil,
      name: "Boston Bruins",
      "ot-losses": "",
      "pct text-success": nil,
      wins: nil,
      year: nil
    }

    assert ateam == expected
  end
end
