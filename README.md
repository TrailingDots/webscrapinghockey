# Web Scraping in Elixir: Hockey Scores

An examples of an Elixir web scraper that handles common 
website interface components. Pagination, web components
parsing and persistent storage elements change the 
URL as you browse. The code uses to popular Elixir 
framework Floki.

## Installation

The installation simply requires cloning this repo to
some directory.

This code illustrates a study for my own learnings in scraping data
from various sites.

If you are interested in scraping websites, each site
must have it's own spider particular to the contents of the
site's html.

Use this code in conjunction with the excellent ebook ["The Ultimate Guide to
Web Scraping](https://blog.hartleybrody.com/guide-to-web-scraping/) by Hartley
Brody. This ebook contains code for python. The advice in this book will repay
itself many times over for the modest price of $15.

This code in this repo provides an elixir implementation 
of Hartley Brody's hockey site for web scraping.

Scraping different sites requires different "spiders" to
properly parse the html. Furthermore, expect the site to change
at unknown intervals. Please refer to the Ultimate Guide
for more advice.

Because this web scaping example targets only a single
site, this spider has been hardcoded into the code.
If you are interested in scraping sites, you understand
that each site is different and must have code specifically
target to that site. Furthermore, real sites are likely
to change anytime and in any way.

I have left debugging statements commented out as an illustration
of how to implement tracing for a changed site. This site
will not change, but a real-world site will. Leaving debug
code in simplifies maintenance of real-world sites.

I wanted to learn Floki better, but was frustrated by the
lack of extensive examples/documentation covering a wide variety
of situations. In particular, the examples of selectors and
attributes does not begin to cover these instances
in the wild. I am impressed at Elixir pattern matching
in overcoming this shortage. Floki properly generated
structure that Elixir pattern matching successfully
used.

A fully-blown database would serve as a basis for further
interpreations, analysis, etc., this code only scrapes
the data and stores it into a Elixir loadable file.

More serious users will likely prefer database output.

# Running the code

~~~ elixir
    script                      # Start capturing output
    iex -S mix
    import Hockey
    the_data = pull_site_data() # Read data from the site
    data = read_site_data()     # Load from file
    data == the_data            # Should be true
    ^C^C                        # Stop the iex shell
    exit                        # Exit the script shell
    clean-typescript            # Clean cruft from typescript (see below)
    vim typescript-processed    # Examine the outut.
    < work with site.data >     # The saved binary output
~~~

# Writing Your Own Interface

The code, in its current state, does not provide a
user interface for selecting information. The current
code does create a persistent file of the site
in loadable Elixir form.

The file <code>site.data</code> has all information
from the scraped pages. To load these into Elixir:

~~~ elixir
    iex -S mix
    import Hockey
    data = read_site_data() # Load the site data
    length(data)            # The number of records saved
    # 607                   # 607 entries on this site.
~~~

As this site provides sample pages for the web scaping
course, it's unlikely this data will change.


# Debugging with script

The log() has been left in the code. When log() returns
true, lots of output results.

The use of lots of log() calls get justified when, not if,
the site changes! Scraping sites should always be performed
defensively as they change without notice.

## clean-typescript

From <code>man script<code>:

> Script places everything in the log file,  including  linefeeds  and
backspaces.  This is not what the naive user expects.


To cleanse your typescript file, place this utility somewhere in your
<code>$PATH</code> and make it executable:

~~~ bash
    #!/bin/bash
    #
    # Command to strip script crud from the typescript output.
    #
    # Run by:
    #   clean_typescript
    @
    # Input is in typescript
    # Output is in typescript-processed
    #
    /usr/bin/cat typescript | \
      /usr/bin/perl -pe 's/\e([^\[\]]|\[.*?[a-zA-Z]|\].*?\a)//g' | \
      /usr/bin/col -b > typescript-processed
    echo output in typescript-processed
~~~

Using <code>script</code> results in an almost unreadable mess.
The output, from <code>typescript-processed</code>, contains only what was
displayed on the screen.

Thanks to [How to clean up output of linux 'script'
command](https://superuser.com/questions/236930/how-to-clean-up-output-of-linux-script-command)
for this incredibly useful utility script.

# To Do

The code does not have a user interface of any kind.
Providing a console UI that selects teams, most and least
numbers, etc. would not be difficult.

Unit tests are mostly missing. Unit testing programs that
interface to the web requires more dedicated testing
procedure than I want to devote at this time. My intent
with this code is to learn about scraping techniques, not
to write a general purpose utility.

A database repository is missing. See the comments
immediately above.

As I consider myself a noobi in Elixir, parts of the
code can be suboptimal. For this I can only offer
excuses.
