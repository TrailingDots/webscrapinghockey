defmodule Tee do
  @moduledoc """
  Place in the pipeline of functions
  and see intermediate results.

  ## Examples

    iex(1)> 1 |> Tee.tap
    tee: 1
    iex(2)> [1,2,3] |> Tee.tap
    tee: [1, 2, 3]
    iex(3)> [1,2,3] |> Tee.tap("debug:")
    debug: [1, 2, 3]
    iex(4)> "Up And Down" |> Tee.tap("all downcase:")
    all downcase: "Up And Down"
    
  """

  # tap a message prefixed by a label.
  def tap(msg, label \\ "") do
    # Attempt to get sensible output
    str_msg = inspect(msg)
    IO.puts("#{label} #{str_msg}")
    msg
  end
end

# Various other Tee commands:
# Enum.map(fn x -> IO.puts x; x end)
# # |> Enum.map(&(IO.puts(&1) && &1))
# Also:
#   dup = fn(x) -> IO.inspect(x); x end
#   1 |>dup.()
#   1
#   [1,2,3] |> dup.()
#   [1,2,3]
