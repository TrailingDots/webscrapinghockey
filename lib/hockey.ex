defmodule Hockey do
  @moduledoc """
  Hockey Teams: Forms, Searching and Pagination.

  Browse through a database of NHL team stats since 1990.
  Practice building a scraper that handles common website
  interface components.

  Take a look at how pagination and
  search elements change the URL as your browse.

  Build a web scraper that can conduct searches and
  paginate through the results.
  """

  @site_data_file "site.data"

  defstruct "diff text-success": "",
            ga: 0,
            gf: 0,
            losses: 0,
            name: "",
            "ot-losses": 0,
            "pct text-success": 0,
            wins: 0,
            year: 0

  _ignore = ~S"""
  @type t :: %Hockey{
          team_name: String.t(),
          year: non_neg_integer,
          losses: non_neg_integer,
          ot_losses: non_neg_integer,
          win_percent: 0,
          goals_for: 0,
          goals_against: 0,
          plus_minus: 0
        }
  """

  @base_url "https://scrapethissite.com/pages/forms/"

  @type html_tree :: tuple | list
  @type url :: String.t()
  @type file :: String.t()

  def debug() do
    # Set to false for no debug logs
    false
  end

  def log(astr) do
    if debug() do
      IO.inspect(astr)
    end
  end

  def base_url() do
    @base_url
  end

  @doc """
  Given a page number, return {:ok, URL} of the hockey site.
  This routine does NOT detect invalid pages!
  Invalid pages get detected when a URL access fails.

  Notice the URLs as you page through the site:
  ["https://scrapethissite.com/pages/forms/",
   "https://scrapethissite.com/pages/forms/?page_num=2",
   "https://scrapethissite.com/pages/forms/?page_num=3",
   ...
  ]
  """
  @spec page_number_to_url(non_neg_integer) :: String.t()
  def page_number_to_url(page_number) when page_number == 0 do
    @base_url
  end

  def page_number_to_url(page_number) when page_number > 0 do
    @base_url <> "?page_num=#{page_number}"
  end

  @doc """
  Fetch and return the raw html from the request site.
  """
  @spec raw_html_from_url() :: any
  def raw_html_from_url() do
    raw_html_from_url(@base_url)
  end

  @spec raw_html_from_url(charlist) :: any
  def raw_html_from_url(url) do
    HTTPoison.start()

    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        body

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts("Not found: #{url}")

      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.puts("#{reason} URL: #{url})")
    end
  end

  @doc """
  Given a URL, load it and parse it.
  Returns {:ok, parsed} or {:error, reason} .
  """
  @spec read_html_and_parse_from_url() :: html_tree
  def read_html_and_parse_from_url() do
    read_html_and_parse_from_url(@base_url)
  end

  @spec read_html_and_parse_from_url(charlist) :: html_tree
  def read_html_and_parse_from_url(url) do
    html = raw_html_from_url(url)
    {:ok, parsed} = Floki.parse_document(html)
    parsed
  end

  @doc """
  Read html from url and save to file
  """
  def read_html_save_to_file(page_num) do
    url = page_number_to_url(page_num)
    html = raw_html_from_url(url)
    page_name = "data/page#{page_num}"
    {:ok, file_handle} = File.open(page_name, [:write])
    IO.write(file_handle, html)
    log("Save to #{page_name}")
  end

  @doc """
  Pull the info from a page and collect all the teams

  Returns:
      {:ok, info_teams}

  raw_teams looks like:
  [
    {"tr", [{"class", "team"}],
    [
     {"td", [{"class", "name"}],
      ["\n                            Chicago Blackhawks\n                        "]},
   ...
  """
  def pull_raw_teams(parsed) do
    Floki.find(parsed, ".team")
  end

  # Ref: https://medium.com/@kay.sackey/create-a-struct-from-a-map-within-elixir-78bf592b5d3b
  # BUG TODO: This overrides defaults in the struct when key is not
  # present in the map.
  def struct_from_map(a_map, as: a_struct) do
    # Find the keys within the map
    keys =
      Map.keys(a_struct)
      |> Enum.filter(fn x -> x != :__struct__ end)

    # Process map, checking for both string / atom keys
    processed_map =
      for key <- keys, into: %{} do
        value = Map.get(a_map, key) || Map.get(a_map, to_string(key))
        {key, value}
      end

    a_struct = Map.merge(a_struct, processed_map)
    a_struct
  end

  @doc """
  Given a raw team, pull the info and create the %Hockey{}
  struct.

  Get the text for each column one at a time. While
  somewhat longer than using text() to grab all columns
  as test and then using split(), this method insures
  that empty columns get processed correctly.

  Return that struct
  """
  def build_team_struct(raw_team) do
    tds = Floki.find(raw_team, "td")
    log("td_column_labels:tds=#{inspect(tds)}")

    result =
      Enum.map(tds, fn td ->
        name = Floki.attribute(td, "class") |> Floki.text()
        value = Floki.text(td) |> String.trim()
        {name, value}
      end)

    result_map = Map.new(result)

    struct_from_map(result_map, as: %Hockey{})
  end

  @doc ~S"""
  Find and return the "Next" url if any.
     {:ok, next_url}
  At the last page return:
     :end
  """
  def next_page(parser) do
    aria_label =
      Floki.find(parser, "a")
      |> Floki.attribute("aria-label")
      |> Floki.text()

    log("aria-label=#{inspect(aria_label)}")

    if aria_label == ["Next"] do
    end
  end

  @doc """
  Build a data structure of all team stats
  from all pages on the site.
  """
  def pull_site_data() do
    pull_site_data(0)
  end

  def pull_site_data(page_number) do
    # Start a list of output data
    # https://stackoverflow.com/questions/25477372/how-to-save-hashdicts-to-file-in-elixir
    site_data = pull_site_data_helper(page_number, [])

    log("\n==== site_data: #{inspect(site_data)}")
    log("pull_site_data: len site_data = #{length(site_data)}")

    # The site_data consists of a list for each page read.
    # Flatten to a single list as pages are irrelevant to scores.
    site_data_flat = List.flatten(site_data)

    # Ref: https://stackoverflow.com/questions/25477372/how-to-save-hashdicts-to-file-in-elixir
    File.write!(@site_data_file, :erlang.term_to_binary(site_data_flat))
    IO.puts("\n=== To load this data:  data = read_site_data()")

    site_data_flat
  end

  @doc """
  Helper fcn for recursive call until end of pages.
  """
  def pull_site_data_helper(page_num, site_data) do
    log("\n=== Starting page #{page_num}")
    url = page_number_to_url(page_num)
    log("\turl=#{inspect(url)}")
    parsed = read_html_and_parse_from_url(url)
    data = pull_page_data(parsed)

    # Append new data to existing site_data.
    site_data = [data | site_data]
    log("length site_data: #{length(site_data)}")

    next_page_num = find_next_page(parsed)

    site_data =
      cond do
        is_integer(next_page_num) ->
          log("pulling site data for page #{next_page_num}")
          pull_site_data_helper(next_page_num, site_data)

        next_page_num == nil ->
          log("length site_data: #{length(site_data)}")
          log("\n==== Read all site_data: #{inspect(site_data)}")
          site_data
      end

    log("End of site data. Last url: #{inspect(url)}")
    log("\n=== helper end. len site_data=#{length(site_data)}")
    site_data
  end

  @doc """
  Read the site data into structs
  """
  def read_site_data(filename \\ @site_data_file) do
    # Ref: https://stackoverflow.com/questions/25477372/how-to-save-hashdicts-to-file-in-elixir
    data = File.read!(filename) |> :erlang.binary_to_term()
    log("read_site_data: #{length(data)}")
    data
  end

  @doc """
  From a parsed page, get the number of the next page.

  I can't figure out the Floki expression for this!

  Hunting for something that looks like:
    _target =
      {"a",
       [{"href", "/pages/forms/?page_num=11"},
          {"aria-label", "Next"}],
       [{"span", [{"aria-hidden", "true"}], ["»"]}]
      }

  Ignore "a" that look like:
    _non_target =
      {"a", [{"href", "/pages/forms/?page_num=24"}],
       [
         "\n                                    \n                                    24\n                                    \n                                "
       ]}

  Return:
      Integer page number of the next page
      nil if no next page.
  """
  def find_next_page(parsed) do
    # Begin by getting all the "a" elements
    a_elts = Floki.find(parsed, "a")

    # Use pattern matching to find and pull apart the next page.
    page =
      Enum.map(a_elts, fn anchor ->
        case anchor do
          {"a", [{"href", page}, {"aria-label", "Next"}], _} ->
            page

          _ ->
            nil
        end
      end)

    # Throw away nils in result
    next_page = Enum.filter(page, fn x -> x != nil end)

    # For a page with pagination to the next page,
    # next_page looks like:
    #    ["/pages/forms/?page_num=11"]
    # Get the page number
    result =
      case length(next_page) do
        1 ->
          next_page
          |> Enum.at(0)
          |> String.split("=")
          |> Enum.at(1)
          |> String.to_integer()

        _ ->
          nil
      end

    result
  end

  def page_number_to_parsed(page_num) when is_number(page_num) do
    url = page_number_to_url(page_num)
    parsed = read_html_and_parse_from_url(url)
    parsed
  end

  @doc """
  Pull the stats from a single page on the site.
  """
  def pull_page_data() do
    parsed = read_html_and_parse_from_url()
    pull_page_data(parsed)
  end

  @doc """
  Given a page number of @base_url, construct the url,
  read and parse the page.
  """

  def pull_page_data(page_num) when is_number(page_num) do
    parsed = page_number_to_parsed(page_num)
    pull_page_data(parsed)
  end

  @doc """
  Given a Floki parsed structure, pull all team info from
  the parsed url.
  """
  def pull_page_data(parsed) do
    raw_teams = pull_raw_teams(parsed)
    # log("raw_teams=#{inspect(raw_teams)}")

    teams_on_page =
      raw_teams
      |> Enum.map(fn ateam -> build_team_struct(ateam) end)

    # work with a single team for now
    # raw_team = Enum.at(raw_teams, 0)
    # build_team_struct(raw_team)

    teams_on_page
  end
end
