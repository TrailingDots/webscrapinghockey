defmodule Hockey.MixProject do
  use Mix.Project

  def project do
    [
      app: :hockey,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      # To run coverage:
      #    MIX_ENV=test mix coveralls.html
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.5.0"},
      {:floki, "~> 0.26.0"},
      {:excoveralls, "~> 0.10", only: :test},
      # To output documentation:  mix docs
      {:ex_doc, "~> 0.22", only: [:dev], runtime: false},
      # From command line:  mix credo
      {:credo, "~> 1.4", only: [:dev], runtime: false}
    ]
  end
end
